# language: en

Feature: Registrarme y darme de alta en el sitio

  Scenario: El usuario se registra y queda listo para ingresar
    Given que no existe el usuario maria@untref.edu.ar
    When intento registrarme como maria@untref.edu.ar
    Then me encuentro en login

  Scenario: El usuario no logra registrarse y puede reintentar
    Given que ya existe el usuario pedro@untref.edu.ar
    When intento registrarme como pedro@untref.edu.ar
    Then me encuentro en nuevo-usuario
      And muestra el mensaje 'El usuario ya existe'

  Rule: Solo puede registrarse si el usuario no existe

    Scenario: Si el usuario no existe en el sitio, se da de alta
      Given que no existe el usuario maria@untref.edu.ar
      When intento registrarme como maria@untref.edu.ar
      Then el usuario se crea

    Scenario: Si el usuario ya existe en el sitio, no se da de alta
      Given que ya existe el usuario pedro@untref.edu.ar
      When intento registrarme como pedro@untref.edu.ar
      Then muestra el mensaje 'El usuario ya existe'

  Rule: El usuario debe ser un email con formato válido

    Scenario: Si el formato de usuario es incorrecto no se da de alta
      Given
      When intento registrarme como untref.edu.ar
      Then el usuario no está registrado
        And muestra el mensaje 'El formato del usuario no es una direccion de email válida'
  
  Rule: El usuario debe ser un email de la untref o un subdominio

    Scenario: Si el usuario se registra con un dominio untref, se da de alta
      Given que no existe el usuario maria@untref.edu.ar
      When intento registrarme como maria@untref.edu.ar
      Then el usuario se crea
    
    Scenario: Si el usuario se registra con un subdominio untref, se da de alta
      Given que no existe el usuario maria@estudiantes.untref.edu.ar
      When intento registrarme como maria@estudiantes.untref.edu.ar
      Then me encuentro en login
    
    Scenario: Si el usuario se registra con un dominio invalido, no se da de alta
      Given que no existe el usuario maria@dominio-invalido.com
      When intento registrarme como maria@dominio-invalido.com
      Then el usuario no está registrado
        And muestra el mensaje 'El dominio debe ser untref.edu.ar (o un subdominio)'

